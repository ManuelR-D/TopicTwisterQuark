using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

public class TestRandomCategory
{
    [Test]
    public void TestRandomCategoryDoesntRepeatCategories()
    {
        List<CategoryDTO> availableCategories = new List<CategoryDTO>();
        for(int i = 0; i < 5; i++)
        {

            var categoryModel = new CategoryDTO();
            switch(i)
            {
                case 0: 
                    categoryModel.Name = "Movies"; 
                    break;
                case 1:
                    categoryModel.Name = "Sports";
                    break;
                case 2:
                    categoryModel.Name = "Countries";
                    break;
                case 3:
                    categoryModel.Name = "Books";
                    break;
                case 4:
                    categoryModel.Name = "Scientists";
                    break;

            }
            availableCategories.Add(categoryModel);
        }

        RandomCategory randomCategoryGenerator = new(availableCategories);
        for(int i = 0; i < 20; i++)
        {
            CategoryDTO randomCategory1 = randomCategoryGenerator.GetRandomCategory();
            CategoryDTO randomCategory2 = randomCategoryGenerator.GetRandomCategory();
            CategoryDTO randomCategory3 = randomCategoryGenerator.GetRandomCategory();
            CategoryDTO randomCategory4 = randomCategoryGenerator.GetRandomCategory();
            CategoryDTO randomCategory5 = randomCategoryGenerator.GetRandomCategory();
            HashSet<string> categoryNames = new()
            {
                randomCategory1.Name,
                randomCategory2.Name,
                randomCategory3.Name,
                randomCategory4.Name,
                randomCategory5.Name
            };

            Assert.IsTrue(categoryNames.Count == 5);
        }

    }
}
