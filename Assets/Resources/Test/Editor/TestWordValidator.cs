﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TestWordValidator
{
    [Test]
    public void TestValidateCaseSensitive()
    {
       
         Dictionary<string, HashSet<string>> _validWords = new Dictionary<string, HashSet<string>>()
       {
            { "Artists", new HashSet<string>() { "Amarillo", "Beige", "Cafe", "Damasco", "Esmeralda", "Frambuesa", "Gris", "Hueso", "Indigo", "Jade", "Kaki", "Lila", "Marron", "Negro", "Oro", "Purpura", "Q", "Rojo", "Salmon", "Turquesa", "Uva", "Violeta", "Zafiro", "W" ,"X" , "Y" } },
            { "Sports", new HashSet<string>() { "Test" } },
            { "Books", new HashSet<string>() { "Test" } },
            { "Countries", new HashSet<string>() { "Test" } },
            { "Movies", new HashSet<string>() { "Test" } },
            { "Videogames", new HashSet<string>() { "Doom" } },
            { "Sciencetists", new HashSet<string>() { "Test" } },
            { "Random", new HashSet<string>() { "Test" } }
        }; 
        WordValidator validator = new WordValidator(_validWords);


        Assert.IsTrue(validator.Validate("Oro", "Artists", 'O'));
        Assert.IsTrue(validator.Validate("oRo", "artIsts", 'o'));
    }
}