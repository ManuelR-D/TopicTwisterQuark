﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEditor.Compilation;
using UnityEngine;

public class TestRepositories
{
    [Test]
    public void TestPlayerRepositoryBinaryFormatter()
    {
        Player p = new Player("Test");

        IPlayerRepository repository = new PlayerRepositoryBinaryFormatter();
        repository.Save(p);
        Player result = repository.GetByName("Test");

        Assert.IsTrue(p.Name == result.Name);
    }

    [Test]
    public void TestCategoryRepositoryLocal()
    {
        Category p = new Category(GameEnum.GAME_CATEGORIES.Movies);
        IWordRepository wordRepo = new WordRepositoryLocal();

        ICategoryRepository repository = new CategoryRepositoryLocal();
        Category result = repository.GetByType(GameEnum.GAME_CATEGORIES.Movies);

        List<Word> expected = p.WordList;
        List<Word> actual = result.WordList;

        Assert.IsTrue(expected.Count == actual.Count);
        Assert.IsTrue(p.CategoryType == result.CategoryType);
        expected.ForEach(x => Assert.IsTrue(actual.Contains(x)));
    }

    [Test]
    public void TestWordRepositoryLocal()
    {
        Word p = new Word("Test", GameEnum.GAME_CATEGORIES.Movies);

        IWordRepository wordRepo = new WordRepositoryLocal();
        List<Word> result = wordRepo.GetByCategoryType(GameEnum.GAME_CATEGORIES.Movies);

        Assert.IsTrue(result.Contains(p));
    }

    [Test]
    public void TestTurnRepositoryBinaryFormatter()
    {
        Player p = new Player("TestPlayer");
        Turn t = new Turn(1, 0, 0, p);

        ITurnRepository turnRepo = new TurnRepositoryBinaryFormatter();
        turnRepo.Save(t);

        Turn saved = turnRepo.GetById(1, 0, 0);

        Assert.IsTrue(t.Equals(saved));
    }

    [Test]
    public void TestRoundRepositoryBinaryFormatter()
    {
        Player p1 = new Player("PlayerOne");
        Player p2 = new Player("PlayerTwo");
        Round r = new Round(1, 0, p1, p2);

        IRoundRepository turnRepo = new RoundRepositoryBinaryFormatter();
        turnRepo.Save(r);

        Round saved = turnRepo.GetById(0, 1);

        Assert.IsTrue(r.Equals(saved));
    }

    [Test]
    public void TestGameRepositoryBinaryFormatter()
    {
        Player p1 = new Player("PlayerOne");
        Player p2 = new Player("PlayerTwo");
        Game g = new Game(new List<Player>() { p1, p2 });

        IGameRepository gameRepo = new GameRepositoryBinaryFormatter();
        gameRepo.Save(g);

        Game saved = gameRepo.GetById(0);

        Assert.IsTrue(g.Equals(saved));
    }

    [Test]
    public void TestLetterRepositoryBinaryFormatter()
    {
        Letter l = new Letter('l');

        ILetterRepository letterRepo = new LetterRepositoryLocal();

        Letter saved = letterRepo.GetByLetter('l');

        Assert.IsTrue(l.Equals(saved));
    }

    [Test]
    public void TestWordRepositorySQL()
    {
        IWordRepository wordRepository = new WordRepositorySQL();
        var aux = wordRepository.GetByCategoryType(GameEnum.GAME_CATEGORIES.Movies);
        Assert.IsTrue(aux != null);
    }

    [Test]
    public void TestPlayerRepositoryService()
    {
        IPlayerRepository playerRepository = new PlayerRepositoryService();
        Player p = playerRepository.Get("test", "test");
        Assert.IsTrue(p.Name == "test" && p.Email == "test@test.com");
    }
}
