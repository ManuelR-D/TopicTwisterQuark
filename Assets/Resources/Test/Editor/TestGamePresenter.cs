using System.Collections.Generic;
using Assets.Scripts.Actions.Validators;
using GamePresenterContract;
using GameViewContract;
using Moq;
using NUnit.Framework;
using UnityEngine.UI;

public class TestGamePresenter
{
    [Test]
    public void TestPresenterSendsValidatedWordsToView()
    {
        /*ResultDTO expectedResults = new ResultDTO(new List<CategoryWidgetDTO>()
        {
            { new CategoryWidgetDTO("C1","W1") },
            { new CategoryWidgetDTO("C2","W2") },
            { new CategoryWidgetDTO("C3","W3") },
            { new CategoryWidgetDTO("C4","W4") },
            { new CategoryWidgetDTO("C5","W5") },
        }, 'W');
        bool[] expectedValidations = new bool[5] { false, true, false, false, true }; 
        Mock<IWordValidator> mockedValidator = new Mock<IWordValidator>();
        mockedValidator.SetupSequence(
            x => x.Validate(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<char>()))
            .Returns(expectedValidations[0])
            .Returns(expectedValidations[1])
            .Returns(expectedValidations[2])
            .Returns(expectedValidations[3])
            .Returns(expectedValidations[4]);

        Dictionary<string, string> categoryUserWordPairs = new Dictionary<string, string>()
        {
            { "C1", "W1" },
            { "C2", "W2" },
            { "C3", "W3" },
            { "C4", "W4" },
            { "C5", "W5" }
        };
        char letter = 'L';
        Mock<IPartidaView> mockedView = new Mock<IPartidaView>();
        mockedView.Setup(x => x.GetCategoryWordPairs()).Returns(categoryUserWordPairs);
        mockedView.Setup(x => x.GetWordLetter()).Returns(letter);

        IPartidaView view = mockedView.Object;
        IWordValidator validator = mockedValidator.Object;
        PartidaPresenter presenter = new PartidaPresenter(view, validator);

        presenter.EndRound();
        mockedView.Verify(x => x.GetCategoryWordPairs(), Times.Once);
        mockedView.Verify(x => x.GetWordLetter(), Times.Once);
        mockedValidator.Verify(x => x.Validate(
            It.IsAny<string>(),
            It.IsAny<string>(),
            It.IsAny<char>()), Times.Exactly(categoryUserWordPairs.Count));
        mockedView.Verify(x => x.UpdateResults(expectedResults), Times.Once);*/
    }
}
