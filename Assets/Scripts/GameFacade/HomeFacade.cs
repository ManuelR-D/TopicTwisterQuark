﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



public class HomeFacade
{
    private Game newGame;
    private Turn actualTurn;
    private IGameRepository gameRepository;
    private IPlayerRepository playerRepository;

    public HomeFacade(int id, IGameRepository gameRepository, IPlayerRepository playerRepository)
    {
        
        this.newGame = gameRepository.GetById(id);
        
        this.playerRepository = playerRepository;

    }


    public void StartGame( string name )
    {
        Player playerOne = new Player(name);
        Player playerTwo = new Player("Bot");

        playerRepository.Save(playerOne);
        playerRepository.Save(playerTwo);

        newGame = new Game(new List<Player>() { playerOne, playerTwo });
        gameRepository = new GameRepositoryBinaryFormatter();
        gameRepository.Save(newGame);

        //SceneSystem.LoadScene(SceneSystem.SceneIndex.Game);
    }
}

