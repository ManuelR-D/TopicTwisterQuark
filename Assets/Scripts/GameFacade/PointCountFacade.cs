﻿using Assets.Scripts.Actions.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public class PointCountFacade
{
    private Game actualGame;
    private Turn actualTurn;
    private Round actualRound;
    private IGameRepository gameRepository;
    private IWordValidator _wordValidator;

    public PointCountFacade(int id, IGameRepository gameRepository)
    {
        this.gameRepository = gameRepository;
        actualGame = gameRepository.GetById(id);
        actualRound = actualGame.GetInProgressRound();
        actualTurn = actualRound.GetTurnInProgress();

        GameObject go = new GameObject("WordValidator");
        go.AddComponent<WordValidatorRequest>();
        this._wordValidator = go.GetComponent<WordValidatorRequest>();
    }


    public int ShowResult(CategoryWidget categories)
    {
        List<CategoryDTO> categoriesDto = new();
        List<string> userWords = new();
        List<CategoryWidgetDTO> results = new();

        for (int i = 0; i < actualTurn.categories.Count; i++)
        {
            categoriesDto.Add(actualTurn.categories[i].AsCategoryDTO());
            userWords.Add(actualTurn.userWords[i].ToString());

            results.Add(new CategoryWidgetDTO(actualTurn.categories[i].ToString(), actualTurn.userWords[i].ToString()));
        }

        categories.SetCategories(categoriesDto);
        categories.SetUserInput(userWords);

        bool[] validation = new bool[5];
        int points = 0;
        int idx = 0;
        foreach (CategoryWidgetDTO category in results)
        {
            bool isValid = _wordValidator.Validate(category.UserInput, category.CategoryTitle, actualTurn.Letter.letter);
            Debug.Log(isValid);
            if (isValid)
            {
                points++;
            }
            validation[idx] = isValid;
            idx++;
        }
        categories.UpdateWithResults(validation);

        actualRound.FinishActualTurn();
        gameRepository.Save(actualGame);
        return points;

    }

}

