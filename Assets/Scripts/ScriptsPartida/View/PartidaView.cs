using GamePresenterContract;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameViewContract
{
    public class PartidaView : MonoBehaviour, IPartidaView
    {
        public TMP_Text CategoryLetter;

        private CategoryWidget CategoryWidget;
        private PartidaPresenter presenter;
        private Button _sendBtn;
        private Timer timer;
        public TMP_Text playerOneName;

        void Start()
        {
            this.CategoryWidget = this.GetComponentInChildren<CategoryWidget>();
            this._sendBtn = this.GetComponentInChildren<Button>();
            this.timer = this.GetComponentInChildren<Timer>();
           


            presenter = new PartidaPresenter(this);
            char randomLetter = presenter.InitializeRandomLetter();
            CategoryLetter.text = randomLetter.ToString();

            List<CategoryDTO> categories = presenter.InitializeCategories();
            SetCategories(categories);
        }

        private void SetCategories(List<CategoryDTO> categories)
        {
            CategoryWidget.SetCategories(categories);
        }

        public Dictionary<string, string> GetCategoryWordPairs()
        {
            return this.CategoryWidget.GetCategoryWordPairs();
        }

        public char GetWordLetter()
        {
            return this.CategoryLetter.text[0];
        }

        public void UpdateResults()
        {
            SceneSystem.LoadScene(SceneSystem.SceneIndex.PointsCount);
        }

        public void SubscribeToSendButton(UnityAction action)
        {
            this._sendBtn.onClick.AddListener(action);
        }

        public void SubscribeToTimer(UnityAction validateWords)
        {
            this.timer.TimeEnd.AddListener(validateWords);
        }

        public void SetPlayerName(string playerName)
        {
            playerOneName.text = playerName;
        }
    }
}

