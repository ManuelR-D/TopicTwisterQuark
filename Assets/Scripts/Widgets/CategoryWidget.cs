using System.Collections.Generic;
using UnityEngine;
using Widgets;

public class CategoryWidget : MonoBehaviour
{
    private CategoryTab[] _categories;

    void Start()
    {
        _categories = this.GetComponentsInChildren<CategoryTab>();
    }

    public void SetCategories(List<CategoryDTO> categories)
    {
        for (int i = 0; i < _categories.Length; i++)
        {
            CategoryTab categoryWidget = _categories[i];
            categoryWidget.SetTextCategory(categories[i].Name);
            categoryWidget.SetImageCateogry(categories[i].Sprite);
        }
    }

    public void SetUserInput(List<string> userInputs)
    {
        for (int i = 0; i < _categories.Length; i++)
        {
            CategoryTab categoryWidget = _categories[i];
            categoryWidget.SetUserWord(userInputs[i]);
        }
    }

    public Dictionary<string,string> GetCategoryWordPairs()
    {
        Dictionary<string, string> result = new();
        foreach (CategoryTab cat in _categories)
        {
            Debug.Log(cat.GetTextCategory() +" -> " + cat.GetUserWord());
            result.Add(cat.GetTextCategory(), cat.GetUserWord());
        }
        return result;
    }

    public void UpdateWithResults(bool[] results)
    {
        int idx = 0;
        foreach (CategoryTab cat in _categories)
        {
            cat.ChangeBackgroundColor(results[idx]);
            idx++;
        }
    }
}
