﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class RepositoryBinaryFormatter<T>
{
    public static T ReadFromFilePath(string path)
    {
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.Open);

            T data = (T)formatter.Deserialize(fs);

            fs.Close();
            return data;
        }
        else
        {
            Debug.LogError("No se encontro el archivo en la ruta: " + path);
            return default(T);
        }
    }

    public static void Save(T entity, string path)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream fs = new FileStream(path, FileMode.Create);

        formatter.Serialize(fs, entity);
        fs.Close();
    }
}
