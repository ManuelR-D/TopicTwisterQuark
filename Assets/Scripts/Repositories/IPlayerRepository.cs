using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerRepository
{
    void Save(Player p);
    Player GetByName(string name);
    Player Get(string name, string password);
    Player GetLocalPlayer();
}
