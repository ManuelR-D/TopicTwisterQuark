﻿using UnityEditor;

public interface IGameRepository
{
    void Save(Game game);
    Game GetById(int id);
}
