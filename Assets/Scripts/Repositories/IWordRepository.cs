﻿using System.Collections.Generic;

public interface IWordRepository
{
    List<Word> GetByCategoryType(GameEnum.GAME_CATEGORIES categoryType);
}
