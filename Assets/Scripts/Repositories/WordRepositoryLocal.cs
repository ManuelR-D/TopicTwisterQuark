﻿using System.Collections.Generic;
using System.Linq;

public class WordRepositoryLocal : IWordRepository
{
    private Dictionary<GameEnum.GAME_CATEGORIES, HashSet<string>> _validWords = new Dictionary<GameEnum.GAME_CATEGORIES, HashSet<string>>()
        {
            { GameEnum.GAME_CATEGORIES.Artists, new HashSet<string>() { "Da Vinci" } },
            { GameEnum.GAME_CATEGORIES.Sports, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Books, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Countries, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Movies, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Videogames, new HashSet<string>() { "Doom" } },
            { GameEnum.GAME_CATEGORIES.Sciencetists, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Random, new HashSet<string>() { "Test" } },
        };

    public List<Word> GetByCategoryType(GameEnum.GAME_CATEGORIES categoryType)
    {
        var validWords = _validWords[categoryType].ToList<string>();
        List<Word> words = new List<Word>();
        foreach(string word in validWords)
        {
            words.Add(new Word(word, categoryType));
        }
        return words;
    }
}