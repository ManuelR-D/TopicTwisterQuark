﻿public interface ITurnRepository
{
    Turn GetById(int turnId, int roundId, int gameId);
    void Save(Turn turn);
}