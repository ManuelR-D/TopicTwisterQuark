﻿using UnityEngine;

public class GameRepositoryBinaryFormatter : IGameRepository
{
    public Game GetById(int id)
    {
        string path = Application.persistentDataPath + "/game" + id + ".bin";
        return RepositoryBinaryFormatter<Game>.ReadFromFilePath(path);
    }

    public void Save(Game game)
    {
        string path = Application.persistentDataPath + "/game" + game.id + ".bin";
        RepositoryBinaryFormatter<Game>.Save(game, path);
    }
}
