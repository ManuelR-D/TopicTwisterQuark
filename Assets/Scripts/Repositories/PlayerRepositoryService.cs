﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerRepositoryService : IPlayerRepository
{
    private const string _host = "https://localhost:7279/";
    private UnityWebRequestAsyncOperation requestState;
    private UnityWebRequest request;
    public Player Get(string name, string password)
    {
        IEnumerator playerCall = GetPlayer(name, password);
        playerCall.MoveNext();
        while (!requestState.isDone)
        {
            Debug.Log("Esperando player");
        }
        string response = request.downloadHandler.text;
        return JsonConvert.DeserializeObject<Player>(response);
    }

    IEnumerator GetPlayer(string name, string password)
    {
        string uri = $"{_host}api/Player?name={name}&password={password}";
        Debug.Log(uri);
        request = UnityWebRequest.Get(uri);
        requestState = request.SendWebRequest();
        yield return requestState;

        if (request.isNetworkError)
        {
            Debug.Log(request.error);
        }

    }

    public Player GetByName(string name)
    {
        throw new NotImplementedException();
    }

    public Player GetLocalPlayer()
    {
        throw new NotImplementedException();
    }

    public void Save(Player p)
    {
        throw new NotImplementedException();
    }
}