﻿public interface ILetterRepository
{
    Letter GetByLetter(char letter);
}