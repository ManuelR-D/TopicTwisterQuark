﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameStateDTO
{
    public int id;
    public List<RoundStateDTO> rounds;

    public GameStateDTO()
    {
        rounds = new List<RoundStateDTO>();
    }
}
