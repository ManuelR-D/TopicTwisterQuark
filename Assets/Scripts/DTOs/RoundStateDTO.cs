﻿using System.Collections.Generic;

[System.Serializable]
public class RoundStateDTO
{
    public enum RoundState
    {
        Tie,
        Win,
        Lose
    }
    //Jorge -> Win
    //Bot -> Lose
    public Dictionary<string, RoundState> PlayerState = new Dictionary<string, RoundState>();
    //Jorge -> 3
    //Bot -> 2
    public Dictionary<string, int> PlayerPoint = new Dictionary<string, int>();
    public RoundStateDTO()
    {
    }
}
