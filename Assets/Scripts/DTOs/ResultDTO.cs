﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResultDTO
{
    public PlayerDTO player;
    //Categoria  -> Resultado
    public List<CategoryWidgetDTO> Categories;
    public char Letter;


    public ResultDTO(List<CategoryWidgetDTO> categories, char letter, PlayerDTO player)
    {
        Categories = categories;
        Letter = letter;
        this.player = player;
    }
}
