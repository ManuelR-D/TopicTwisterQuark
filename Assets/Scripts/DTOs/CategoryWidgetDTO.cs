﻿using System;
using UnityEngine;

[Serializable]
public class CategoryWidgetDTO
{
    public string CategoryTitle {get; set;}
    public string UserInput { get; set; }

    public CategoryWidgetDTO(string categoryTitle, string userInput) 
    {
        CategoryTitle = categoryTitle;
        UserInput = userInput;
    }
}
