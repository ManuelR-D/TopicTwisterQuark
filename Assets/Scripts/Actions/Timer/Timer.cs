using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{

    public float time;

    public TMP_Text text;

    public UnityEvent TimeEnd = new UnityEvent();

    void Start()
    {
        InvokeRepeating("Timing", 0f, 1f);
    }

    

    void Timing()
    {
        time -= 1;
        text.text = time.ToString();
        if(time == 0f)
        {
            TimeEnd.Invoke();
            CancelInvoke();
        }
    }
}
