﻿using Assets.Scripts.Actions.Validators;
using Codice.CM.Client.Differences;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.Networking;

public class WordValidatorRequest : MonoBehaviour, IWordValidator
{
    //static readonly HttpClient client = new HttpClient();
    private const string _host = "https://localhost:7279/";
    private bool result = false;
    private UnityWebRequestAsyncOperation requestState;
    private UnityWebRequest request;
    public WordValidatorRequest()
    {

    }

    public bool Validate(string word, string category, char letter)
    {
        IEnumerator request = (ValidateRequest(word, category, letter));
        request.MoveNext();
        while(!requestState.isDone)
        {
            Debug.Log("Waiting...");
        }
        result = this.request.downloadHandler.text.ToLower() == "true" ? true : false;
        return result;
    }

    IEnumerator ValidateRequest(string word, string category, char letter)
    {
        //IsValidWord?word=test&initialLetter=t&categoryType=movies
        string uri = $"{_host}api/Word/IsValidWord?categoryType={category}&initialLetter={letter}&word={word}";
        Debug.Log(uri);
        request = UnityWebRequest.Get(uri);
        requestState = request.SendWebRequest();
        yield return requestState;

        if (request.isNetworkError)
        {
            Debug.Log(request.error);
            result = false;
        }
        else
        {
            // Show results as text
            Debug.Log(request.downloadHandler.text);

            string responseBody = request.downloadHandler.text;
            result = responseBody == "True" ? true : false;
        }
    }

}
