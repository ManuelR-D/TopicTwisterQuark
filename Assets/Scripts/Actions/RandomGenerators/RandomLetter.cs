using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomLetter
{
    private static string _availableLetters = "abcdefghijklmn�opqrstuvwxyz";
    public RandomLetter()
    {

    }

    public RandomLetter(string availableLetters)
    {
        _availableLetters = availableLetters;
    }
    public LetterDTO GenerateRandomLetter()
    {
        return new LetterDTO(_availableLetters[Random.Range(0, _availableLetters.Length)]);
    }
}
