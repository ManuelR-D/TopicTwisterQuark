﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class CategoryDTOFactory
    {
        private static readonly string spritesPath = "SpriteCategories/";
        public static CategoryDTO CreateFromCategoryName(string categoryName)
        {
            var resources = Resources.LoadAll<Sprite>(spritesPath);

            var categoryModel = new CategoryDTO();
            categoryModel.Name = categoryName;
            
            foreach (Sprite categorySprite in resources)
            {
                if(categorySprite.name == categoryName)
                {
                    categoryModel.Sprite = categorySprite;
                }
            }
            return categoryModel;
        }

        public static List<CategoryDTO> CreateAllCategories()
        {
            List<CategoryDTO> categories = new List<CategoryDTO>();
            var resources = Resources.LoadAll<Sprite>(spritesPath);
            foreach (Sprite category in resources)
            {
                var categoryModel = new CategoryDTO();
                categoryModel.Name = category.name;
                categoryModel.Sprite = category;
                categories.Add(categoryModel);
            }
            return categories;
        }
    }
}
