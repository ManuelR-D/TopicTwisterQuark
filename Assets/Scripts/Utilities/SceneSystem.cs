using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSystem
{
    public enum SceneIndex
    {
        Home,
        Game,
        PointsCount,
        Winner,
        EndGame
    }
    public static void LoadScene(SceneIndex idx) => SceneManager.LoadScene((int)idx);
}
