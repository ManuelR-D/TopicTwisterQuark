using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PointCountView : MonoBehaviour, IPointCountView
{
    public TMP_Text PointCountText;
    public Button NextPointCountBtn;

    private CategoryWidget _categoryWidget;
    private PointCountPresenter _presenter;

    public CategoryWidget GetCategoryWidget()
    {
        return _categoryWidget;
    }

    public void SetPoints(int points)
    {
        PointCountText.text = points.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        _categoryWidget = GetComponentInChildren<CategoryWidget>();
        _presenter = new PointCountPresenter(this);
        _presenter.ShowResults();
        NextPointCountBtn.onClick.AddListener(GenerateBotResult);
    }

    //Bot results at the moment
    void GenerateBotResult()
    {
        _presenter.GenerateBotTurn();
    }

}
