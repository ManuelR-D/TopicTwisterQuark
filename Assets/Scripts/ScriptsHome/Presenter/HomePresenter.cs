using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomePresenter 
{
    private IHomeView _view;
    private HomeFacade homeFacade = new(0, new GameRepositoryBinaryFormatter(), new PlayerRepositoryBinaryFormatter() );

    public HomePresenter(IHomeView homeView)
    {
        _view = homeView;
        _view.GetButton().onClick.AddListener(StartArcade);
    }

    public void StartArcade()
    {
        string name = _view.GetInputName();
        homeFacade.StartGame(name);
        SceneSystem.LoadScene(SceneSystem.SceneIndex.Game);

    }
}
